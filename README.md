# 🎙 Vue Demo – a Demo app to showcase some Vue scenarios

[[_TOC_]]

**Note that some of the code here is not meant to be a production code**

## Build locally

To work locally with this project, you'll have to follow the steps below:

-   Fork, clone or download this project
-   Install the dependencies by running `npm i`
-   Generate and preview the website with hot-reloading by running `npm run dev`
-   Play with errors

## Scenarios

### Vue Errors management scenarios

Error management in frontend applications can be tricky and cumbersome. This project is meant to explore
an approach to error handling which takes advantage of two Vue API methods:

-   [errorHandler](https://vuejs.org/v2/api/#errorHandler)
-   [errorCaptured](https://vuejs.org/v2/api/#errorCaptured)

Read more about them in the [Vue documentation](https://vuejs.org/v2/api/).

#### Sentry Vue Integration

The `errorHandler` implementation is managed via the Vue [Sentry Integration](https://docs.sentry.io/platforms/javascript/configuration/integrations/plugin/).

## Credits

&copy; [Angelo Gulina](https://gitlab.com/agulina) – 2021
