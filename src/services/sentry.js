import Vue from 'vue';
import * as Sentry from '@sentry/browser';
import * as SentryIntegrations from '@sentry/integrations';

const denyUrls = [
    // Chrome extensions
    /extensions\//i,
    /^(?:chrome|moz)(?:-extension)?:\/\//i,
];

const fakeSentryDsn = 'https://aaaaaaa@sentry.gitlab.net/10';

/*
    Note: this would not be needed in production code.
    It's handled here like this to allow the toggling on/off of some Sentry related configuration.
 */
export const initSentry = (() => {
    const integrations = [
        ...Sentry.defaultIntegrations,
        new SentryIntegrations.Dedupe(),
        new SentryIntegrations.ExtraErrorData({ depth: 3 }),
        new SentryIntegrations.Vue({ Vue, attachProps: true }),
    ];

    Sentry.init({
        dsn: fakeSentryDsn,
        denyUrls,
        integrations,
    });

    let VueConfigError = Vue.config.errorHandler;

    return (isActive, isFiltered) => {
        const integrations = [
            ...Sentry.defaultIntegrations,
            new SentryIntegrations.Dedupe(),
            new SentryIntegrations.ExtraErrorData({ depth: 3 }),
            isActive && new SentryIntegrations.Vue({ Vue, attachProps: true }),
        ].filter(Boolean);

        Sentry.init({
            dsn: fakeSentryDsn,
            ignoreErrors: isFiltered ? [/Cannot read property/] : [],
            denyUrls,
            integrations,
        });

        // restoring Vue errorHandler
        if (!isActive) {
            Vue.config.errorHandler = null;
        } else {
            Vue.config.errorHandler = VueConfigError;
        }
    };
})();
