import setConfigs from '@gitlab/ui/dist/config';
import Vue from 'vue';
import App from './app.vue';

setConfigs();

const rootAppId = '#app';
document.addEventListener('DOMContentLoaded', () => {
    new Vue({
        el: rootAppId,
        render: createElement => createElement(App),
    });
});
