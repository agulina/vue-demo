const HtmlPlugin = require('html-webpack-plugin');
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const webpack = require('webpack');

const scriptsPath = path.resolve(__dirname, './');
const srcBasePath = `${scriptsPath}/src`;
const destPath = `${scriptsPath}/public`;

module.exports = {
    devServer: {
        contentBase: destPath,
        historyApiFallback: true,
        hot: true,
        inline: true,
        port: 9000,
        watchOptions: {
            ignored: /node_modules/,
        },
    },
    entry: {
        main: `${srcBasePath}/index.js`,
    },
    performance: { hints: false },
    module: {
        rules: [
            {
                test: /\.vuex?$/,
                loader: 'vue-loader',
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
            },
            {
                test: /\.(s*)[a|c]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    'vue-style-loader',
                    // Translates CSS into CommonJS
                    'css-loader',
                    // Compiles Sass to CSS
                    {
                        loader: 'sass-loader',
                        options: {
                            sassOptions: {
                                includePaths: ['node_modules'],
                            },
                        },
                    },
                    {
                        loader: 'style-resources-loader',
                        options: {
                            patterns: [
                                'node_modules/@gitlab/ui/src/scss/variables.scss',
                                'node_modules/@gitlab/ui/src/scss/utility-mixins/*.scss',
                            ],
                        },
                    },
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: ['file-loader'],
            },
        ],
    },
    output: {
        path: destPath,
        filename: '[name][hash].js',
    },
    plugins: [
        new HtmlPlugin({
            template: `${srcBasePath}/index.html`,
            favicon: `${srcBasePath}/favicon.png`,
        }),
        new webpack.IgnorePlugin(/moment/, /pikaday/),
        new VueLoaderPlugin(),
    ],
    resolve: {
        extensions: ['*', '.ts', '.js', '.vue', '.json', '.mjs'],
        alias: {
            vue$: 'vue/dist/vue.esm.js',
        },
    },
};
